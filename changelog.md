# Changelog

## 1.3.3
- Added character level-up form
- Added the ability to edit protected characters
- A few minor fixes

## 1.3.2
- Added background/extra language selection
- Added new step in character creation
- Multiple fixes

## 1.3.1
- Added Changelog

## 1.3.0
- Added a Game Master screen
- Added Worlds
- Campaigns are now Parties
- Added Languages to Characters
- Added a new simplified interface for Character creation
- Added language selection (French and English)
- Added theme selection (manual instead of being linked to the browser)
- Many form fixes

## 1.2.1
- Lots of minor fixes to interfaces

## 1.2.0
- Added Campaigns
- Added Languages
- Added Languages to Races
- Added maximum hit points to Character edition

## 1.1.0
- Re-made Character edition interface
- Re-made Character access management
- Added filters on list views
- Added modifiers to the Character edition interface

## 1.0.0
- Initial public release
- Re-made Races
- Re-made Classes
- Re-made Backgrounds

## Alpha
- Initial version
- Added Races
- Added Classes
- Added Backgrounds
- Added Characters
