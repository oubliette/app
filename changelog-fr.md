# Journal des changements

## 1.3.3
- Ajout d'un formulaire de passe à niveau de personnage
- Ajout de l'habileté de modifier des personnages protégés
- Quelques correctifs mineurs

## 1.3.2
- Ajout de la sélection des langues d'historiques/extra
- Ajout d'une étape dans la création de personnage
- Plusieurs correctifs

## 1.3.1
- Ajout du journal des changements

## 1.3.0
- Ajout de l'écran de maître de jeu
- Ajout des mondes
- Les campagnes sont maintenant des équipes
- Ajout des langues aux personnages
- Ajout d'une interface simplifiée pour créer les personnages
- Ajout de la sélection de langue (français et anglais)
- Ajout de la sélection de thème (manuel plutôt que lié au navigateur)
- Correctifs sur les formulaires

## 1.2.1
- Nombreux correctifs mineurs sur les interfaces

## 1.2.0
- Ajout des campagnes
- Ajout des langues
- Ajout des langues aux races
- Ajout du calcul des points de vie maximums dans l'interface d'édition des personnages

## 1.1.0
- Refonte de l'interface d'édition des personnages
- Refonte de la gestion des accès aux personnages
- Ajout des filtres sur les interfaces de liste
- Ajout du calcul des modificateurs dans l'interface d'édition des personnages

## 1.0.0
- Sortie initiale au public
- Refonte des races
- Refonte des classes
- Refonte des historiques

## Alpha
- Version initiale
- Ajout des races
- Ajout des classes
- Ajout des historiques
- Ajout des personnages
