import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import { getters, moduleName } from '@/store/global/names'
import AppWrapper from '@/views/AppWrapper.vue'
import Home from '@/views/Home.vue'
import Account from '@/views/Account.vue'
import Login from '@/views/Login.vue'
import Register from '@/views/Register.vue'
import RecoverPassword from '@/views/RecoverPassword.vue'
import ResetPassword from '@/views/ResetPassword.vue'
import Confirm from '@/views/Confirm.vue'
import ResolveInvitation from '@/views/ResolveInvitation.vue'
import Logout from '@/views/Logout.vue'
import PartyList from '@/views/PartyList.vue'
import Party from '@/views/Party.vue'
import ViewParty from '@/views/ViewParty.vue'
import CharacterList from '@/views/CharacterList.vue'
import Character from '@/views/Character.vue'
import CreateCharacter from '@/views/CreateCharacter.vue'
import ViewCharacter from '@/views/ViewCharacter.vue'
import PrintCharacter from '@/views/PrintCharacter.vue'
import Background from '@/views/Background.vue'
import BackgroundList from '@/views/BackgroundList.vue'
import Class from '@/views/Class.vue'
import ClassList from '@/views/ClassList.vue'
import Race from '@/views/Race.vue'
import RaceList from '@/views/RaceList.vue'
import Language from '@/views/Language.vue'
import LanguageList from '@/views/LanguageList.vue'
import World from '@/views/World.vue'
import DungeonMaster from '@/views/DungeonMaster.vue'
import NotFound from '@/views/NotFound.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/characters/:id/print',
    name: 'Print Character',
    component: PrintCharacter,
    meta: {
      ignoreAuthentication: true
    }
  },
  {
    path: '/',
    component: AppWrapper,
    meta: {
      ignoreAuthentication: true
    },
    children: [
      // User
      {
        path: '/account',
        name: 'Account',
        component: Account
      },
      {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
          ignoreAuthentication: true
        }
      },
      {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
          ignoreAuthentication: true
        }
      },
      {
        path: '/recover-password',
        name: 'Recover Password',
        component: RecoverPassword,
        meta: {
          ignoreAuthentication: true
        }
      },
      {
        path: '/reset-password',
        name: 'Reset Password',
        component: ResetPassword,
        meta: {
          ignoreAuthentication: true
        }
      },
      {
        path: '/confirm',
        name: 'Confirm',
        component: Confirm,
        meta: {
          ignoreAuthentication: true
        }
      },
      {
        path: '/logout',
        name: 'Logout',
        component: Logout
      },
      {
        path: '/confirm-invitation',
        name: 'Resolve Invitation',
        component: ResolveInvitation
      },
      // Game
      {
        path: '/backgrounds',
        name: 'Backgrounds',
        component: BackgroundList
      },
      {
        path: '/backgrounds/new',
        name: 'New Background',
        component: Background
      },
      {
        path: '/backgrounds/:id',
        name: 'Edit Background',
        component: Background
      },
      {
        path: '/parties',
        name: 'Parties',
        component: PartyList
      },
      {
        path: '/parties/new',
        name: 'New Party',
        component: Party
      },
      {
        path: '/parties/:id/edit',
        name: 'Edit Party',
        component: Party
      },
      {
        path: '/parties/:id/view',
        name: 'View Party',
        component: ViewParty
      },
      {
        path: '/characters',
        name: 'Characters',
        component: CharacterList
      },
      {
        path: '/characters/advanced',
        name: 'New Character Advanced',
        component: Character
      },
      {
        path: '/characters/new',
        name: 'New Character',
        component: CreateCharacter
      },
      {
        path: '/characters/:id/edit',
        name: 'Edit Character',
        component: Character
      },
      {
        path: '/characters/:id/view',
        name: 'View Character',
        component: ViewCharacter,
        meta: {
          ignoreAuthentication: true
        }
      },
      {
        path: '/classes',
        name: 'Classes',
        component: ClassList
      },
      {
        path: '/classes/new',
        name: 'New Class',
        component: Class
      },
      {
        path: '/classes/:id',
        name: 'Edit Class',
        component: Class
      },
      {
        path: '/races',
        name: 'Races',
        component: RaceList
      },
      {
        path: '/races/new',
        name: 'New Race',
        component: Race
      },
      {
        path: '/races/:id',
        name: 'Edit Race',
        component: Race
      },
      {
        path: '/languages',
        name: 'Languages',
        component: LanguageList
      },
      {
        path: '/languages/new',
        name: 'New Language',
        component: Language
      },
      {
        path: '/languages/:id',
        name: 'Edit Language',
        component: Language
      },
      {
        path: '/world/new',
        name: 'New World',
        component: World
      },
      {
        path: '/world/:id',
        name: 'Edit World',
        component: World
      },
      {
        path: '/dm/:id',
        name: 'Dungeon Master',
        component: DungeonMaster
      }
    ]
  },
  {
    path: '/notfound',
    name: 'Not Found',
    component: NotFound,
    meta: {
      ignoreAuthentication: true
    }
  },
  {
    path: '/*',
    redirect: {
      name: 'Not Found'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: import.meta.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = 'Oubliette'
  const isAuthenticated = store.getters[`${moduleName}/${getters.isAuthenticated}`]

  if (isAuthenticated || to.meta?.ignoreAuthentication) {
    const invitation = localStorage.getItem('invitation')

    if (invitation && isAuthenticated) {
      localStorage.removeItem('invitation')
      return next({ name: 'Resolve Invitation', query: JSON.parse(invitation) })
    }

    return next()
  } else {
    if (to.name === 'Resolve Invitation') {
      localStorage.setItem('invitation', JSON.stringify(to.query))
    }

    return next({ name: 'Login' })
  }
})

export default router
