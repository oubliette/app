import { Base64 } from 'js-base64'

export const parseJwt = jwt => {
  if (typeof jwt === 'string') {
    const parts = jwt.split('.')

    if (parts.length === 3) {
      const dataPart = parts[1]

      try {
        const decodedData = Base64.decode(dataPart)
        const parsedData = JSON.parse(decodedData)
        return parsedData
      } catch (error) {}
    }
  }

  return null
}
