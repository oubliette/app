import Vue from 'vue'

Vue.directive('fit-text', {
  inserted (el, params) {
    let isOverflow = el.clientHeight < el.scrollHeight
    const style = window.getComputedStyle(el, null)
    let fontSize = parseFloat(style.getPropertyValue('font-size'))

    while (isOverflow && fontSize > 1) {
      fontSize -= 1
      el.style.fontSize = `${fontSize}px`
      isOverflow = el.clientHeight < el.scrollHeight
    }
  }
})
