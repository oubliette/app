export default class RaceSummary {
  constructor (json) {
    this.id = json.id
    this.updatedAt = new Date(json.updatedAt)
    this.name = json.name
  }
}
