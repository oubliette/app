export default class Language {
  constructor () {
    this.id = null

    this.name = ''
    this.description = ''
    this.exotic = false
    this.script = ''
    this.typicalSpeakers = ''
  }

  static from (json) {
    const language = new Language()
    language.id = json.id
    language.createdAt = json.createdAt
    language.updatedAt = json.updatedAt

    language.name = json.name
    language.description = json.description
    language.exotic = json.exotic
    language.script = json.script
    language.typicalSpeakers = json.typicalSpeakers

    return language
  }

  static toJSON (language) {
    return {
      id: language.id,
      name: language.name,
      description: language.description,
      exotic: language.exotic,
      script: language.script,
      typicalSpeakers: language.typicalSpeakers
    }
  }
}
