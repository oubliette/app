export default class CharacterAccess {
  constructor () {
    this.id = null
    this.email = ''
    this.grantedAt = ''
    this.name = ''
  }

  static from (json) {
    const character = new CharacterAccess()
    character.id = json.id
    character.email = json.email
    character.grantedAt = new Date(json.grantedAt)
    character.name = json.name

    return character
  }
}
