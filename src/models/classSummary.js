export default class ClassSummary {
  constructor (json) {
    this.id = json.id
    this.updatedAt = new Date(json.updatedAt)
    this.name = json.name
    this.hitDie = json.hitDie
  }
}
