import { exceptDefault, exceptNullOrEmpty } from '@/utils'
import Attack from '@/models/attack'
import Bonus from '@/models/bonus'

export default class Character {
  constructor () {
    this.id = null
    this.owned = true

    this.name = ''
    this.player = ''
    this.alignment = null
    this.experience = 0
    this.raceId = null
    this.backgroundId = null

    this.abilities = {}
    this.skillProficiencies = {}
    this.saveProficiencies = {}
    this.currentHitPoints = 0
    this.temporaryHitPoints = 0
    this.deathSaveSuccesses = 0
    this.deathSaveFailures = 0
    this.inspiration = false
    this.archived = false
    this.otherProficiencies = ''

    this.attacks = []
    this.equipment = ''

    this.features = ''
    this.bonuses = []

    this.spellcasting = ''

    this.description = ''
    this.personality = ''
    this.ideals = ''
    this.bonds = ''
    this.flaws = ''
    this.languageIds = []

    this.confidentiality = 'Private'
  }

  static from (json) {
    const character = new Character()
    character.id = json.id
    character.owned = json.owned
    character.createdAt = json.createdAt
    character.updatedAt = json.updatedAt

    character.name = json.name
    character.player = json.player
    character.alignment = json.alignment
    character.experience = json.experience
    character.raceId = json.raceId
    character.backgroundId = json.backgroundId

    character.abilities = json.abilities ?? {}
    character.skillProficiencies = json.skillProficiencies ?? {}
    character.saveProficiencies = json.saveProficiencies ?? {}
    character.currentHitPoints = json.currentHitPoints
    character.temporaryHitPoints = json.temporaryHitPoints
    character.deathSaveSuccesses = json.deathSaveSuccesses
    character.deathSaveFailures = json.deathSaveFailures
    character.inspiration = json.inspiration
    character.archived = json.archived
    character.otherProficiencies = json.otherProficiencies

    character.attacks = json.attacks.map(x => Attack.from(x))
    character.equipment = json.equipment

    character.features = json.features
    character.bonuses = json.bonuses.map(x => Bonus.from(x))

    character.spellcasting = json.spellcasting

    character.description = json.description
    character.personality = json.personality
    character.ideals = json.ideals
    character.bonds = json.bonds
    character.flaws = json.flaws
    character.languageIds = json.languageIds ?? []

    character.confidentiality = json.confidentiality

    return character
  }

  static toJSON (character) {
    return {
      id: character.id,
      name: character.name,
      player: character.player,
      alignment: character.alignment,
      experience: character.experience,
      raceId: character.raceId,
      backgroundId: character.backgroundId,
      abilities: exceptNullOrEmpty(character.abilities),
      skillProficiencies: exceptNullOrEmpty(character.skillProficiencies),
      saveProficiencies: exceptNullOrEmpty(character.saveProficiencies),
      currentHitPoints: character.currentHitPoints,
      temporaryHitPoints: character.temporaryHitPoints,
      deathSaveSuccesses: character.deathSaveSuccesses,
      deathSaveFailures: character.deathSaveFailures,
      inspiration: character.inspiration,
      archived: character.archived,
      otherProficiencies: character.otherProficiencies,
      attacks: exceptDefault(character.attacks, new Attack()).map(x => Attack.toJSON(x)),
      equipment: character.equipment,
      features: character.features,
      bonuses: exceptDefault(character.bonuses, new Bonus()).map(x => Bonus.toJSON(x)),
      spellcasting: character.spellcasting,
      description: character.description,
      personality: character.personality,
      ideals: character.ideals,
      bonds: character.bonds,
      flaws: character.flaws,
      languageIds: character.languageIds,
      confidentiality: character.confidentiality
    }
  }
}
