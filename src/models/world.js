export default class World {
  constructor () {
    this.id = null

    this.name = ''
    this.description = ''
    this.alias = ''
  }

  static from (json) {
    const world = new World()
    world.id = json.id
    world.createdAt = json.createdAt
    world.updatedAt = json.updatedAt

    world.name = json.name
    world.description = json.description
    world.alias = json.alias

    return world
  }

  static toJSON (world) {
    return {
      id: world.id,
      name: world.name,
      description: world.description,
      alias: world.alias
    }
  }
}
