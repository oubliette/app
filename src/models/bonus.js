export default class Bonus {
  constructor () {
    this.description = ''
    this.target = ''
    this.type = 'Ability'
    this.value = 0
  }

  static from (json) {
    const bonus = new Bonus()
    bonus.description = json.description
    bonus.target = json.target
    bonus.type = json.type
    bonus.value = json.value
    return bonus
  }

  static toJSON (bonus) {
    return {
      description: bonus.description,
      target: bonus.target,
      type: bonus.type,
      value: bonus.value
    }
  }
}
