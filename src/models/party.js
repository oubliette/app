export default class Party {
  constructor () {
    this.id = null

    this.name = ''
    this.description = ''
  }

  static from (json) {
    const party = new Party()
    party.id = json.id
    party.createdAt = json.createdAt
    party.updatedAt = json.updatedAt

    party.name = json.name
    party.description = json.description

    return party
  }

  static toJSON (party) {
    return {
      id: party.id,
      name: party.name,
      description: party.description
    }
  }
}
