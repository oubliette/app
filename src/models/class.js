export default class Class {
  constructor () {
    this.id = null

    this.name = ''
    this.description = ''
    this.hitDie = null
    this.armors = ''
    this.tools = ''
    this.weapons = ''
    this.saveProficiencies = []

    this.skillCount = null
    this.skillChoices = []
    this.startingWealth = ''
    this.equipment = []
  }

  static from (json) {
    const _class = new Class()
    _class.id = json.id
    _class.createdAt = json.createdAt
    _class.updatedAt = json.updatedAt

    _class.name = json.name
    _class.description = json.description
    _class.hitDie = json.hitDie
    _class.armorProficiencies = json.armorProficiencies[0]
    _class.toolProficiencies = json.toolProficiencies[0]
    _class.weaponProficiencies = json.weaponProficiencies[0]
    _class.saveProficiencies = json.saveProficiencies

    _class.skillCount = json.skillProficiencies?.count ?? 0
    _class.skillChoices = json.skillProficiencies?.list ?? []
    _class.startingWealth = json.startingWealth
    _class.equipment = json.equipment
    return _class
  }

  static toJSON (_class) {
    return {
      id: _class.id,
      name: _class.name,
      description: _class.description,
      hitDie: _class.hitDie,
      armorProficiencies: [_class.armorProficiencies],
      toolProficiencies: [_class.toolProficiencies],
      weaponProficiencies: [_class.weaponProficiencies],
      saveProficiencies: _class.saveProficiencies,
      skillProficiencies: {
        count: _class.skillCount || null,
        list: _class.skillChoices
      },
      startingWealth: _class.startingWealth,
      equipment: _class.equipment
    }
  }
}
