export default class CharacterSummary {
  constructor (json) {
    this.id = json.id
    this.name = json.name
    this.race = json.race
    this.background = json.background
    this.player = json.player
    this.classes = json.classes
  }
}
