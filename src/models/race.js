export default class Race {
  constructor () {
    this.id = null

    this.name = ''
    this.description = ''
    this.extraSkills = 0
    this.extraFeats = 0
    this.names = []
    this.namesText = ''

    this.abilities = {}
    this.abilityScoreIncreaseText = ''
    this.extraAbilities = 0
    this.ageText = ''
    this.alignmentText = ''
    this.languageIds = []
    this.languagesText = ''
    this.extraLanguages = 0
    this.sizeText = ''
    this.size = 'Medium'
    this.speedText = ''
    this.speeds = {}

    // this.traits = []
  }

  static from (json) {
    const race = new Race()
    race.id = json.id
    race.createdAt = json.createdAt
    race.updatedAt = json.updatedAt

    race.name = json.name
    race.description = json.description
    race.extraSkills = json.extraSkills
    race.extraFeats = json.extraFeats
    race.names = Object.entries(json.names).map(([name, names]) => ({ name, names }))
    race.namesText = json.namesText

    race.abilities = json.abilities ?? {}
    race.abilityScoreIncreaseText = json.abilityScoreIncreaseText
    race.extraAbilities = json.extraAbilities
    race.ageText = json.ageText
    race.alignmentText = json.alignmentText
    race.languageIds = json.languageIds
    race.languagesText = json.languagesText
    race.extraLanguages = json.extraLanguages
    race.sizeText = json.sizeText
    race.size = json.size
    race.speedText = json.speedText
    race.speeds = json.speeds ?? {}

    /* race.traits = json.traits.map(value => {
      if (value.speeds) {
        value.speeds = Object.entries(value.speeds).map(([type, value]) => ({ type, value }))[0]
      }

      return value
    }) */

    return race
  }

  static toJSON (race) {
    return {
      id: race.id,
      name: race.name,
      namesText: race.namesText,
      names: race.names.reduce((acc, value) => {
        acc[value.name] = value.names
        return acc
      }, {}),
      description: race.description,
      extraSkills: race.extraSkills,
      extraFeats: race.extraFeats,
      abilities: race.abilities,
      abilityScoreIncreaseText: race.abilityScoreIncreaseText,
      extraAbilities: race.extraAbilities,
      ageText: race.ageText,
      alignmentText: race.alignmentText,
      languageIds: race.languageIds,
      languagesText: race.languagesText,
      extraLanguages: race.extraLanguages,
      sizeText: race.sizeText,
      size: race.size,
      speedText: race.speedText,
      speeds: race.speeds
      /* traits: race.traits.map(value => {
        if (value.speeds) {
          value.speeds = { [value.speeds.type]: value.speeds.value }
        }

        return value
      }) */
    }
  }
}
