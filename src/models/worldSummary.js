export default class WorldSummary {
  constructor (json) {
    this.id = json.id
    this.updatedAt = new Date(json.updatedAt)
    this.alias = json.alias
    this.name = json.name
  }
}
