export default class PartySummary {
  constructor (json) {
    this.id = json.id
    this.updatedAt = new Date(json.updatedAt)
    this.name = json.name
  }
}
