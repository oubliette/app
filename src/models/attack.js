import { parseDice, getDice } from '@/parseDice'

export default class Attack {
  constructor () {
    this.ability = null
    this.attackBonus = 0
    this.damage = ''
    this.damageType = ''
    this.description = ''
    this.name = ''
    this.proficiency = false
  }

  static from (json) {
    const attack = new Attack()
    attack.ability = json.ability
    attack.attackBonus = json.attackBonus
    attack.damage = getDice({ dice: json.hitDice, die: json.hitDie, bonus: json.damageBonus })
    attack.damageType = json.damageType
    attack.description = json.description
    attack.name = json.name
    attack.proficiency = json.proficiency
    return attack
  }

  static toJSON (attack) {
    const { dice, die, bonus } = parseDice(attack.damage)
    return {
      ability: attack.ability,
      attackBonus: attack.attackBonus,
      damageBonus: bonus,
      damageType: attack.damageType,
      description: attack.description,
      hitDice: dice,
      hitDie: die,
      name: attack.name,
      proficiency: attack.proficiency
    }
  }
}
