export default class CharacterSheet {
  constructor () {
    this.id = 0
    this.editable = false
    this.owned = false
    this.world = ''

    this.abilities = {
      Strength: { score: 10, modifier: 0 },
      Dexterity: { score: 10, modifier: 0 },
      Constitution: { score: 10, modifier: 0 },
      Intelligence: { score: 10, modifier: 0 },
      Wisdom: { score: 10, modifier: 0 },
      Charisma: { score: 10, modifier: 0 }
    }
    this.alignment = 'None'
    this.attacks = []
    this.background = ''
    this.classes = {}
    this.experience = 0
    this.inspiration = false
    this.name = ''
    this.passiveWisdom = 10
    this.player = ''
    this.proficiencyBonus = 2
    this.race = ''
    this.savingThrows = {
      Strength: { bonus: 0, proficiency: false },
      Dexterity: { bonus: 0, proficiency: false },
      Constitution: { bonus: 0, proficiency: false },
      Intelligence: { bonus: 0, proficiency: false },
      Wisdom: { bonus: 0, proficiency: false },
      Charisma: { bonus: 0, proficiency: false }
    }
    this.skills = {
      Acrobatics: { bonus: 0, proficiency: false },
      AnimalHandling: { bonus: 0, proficiency: false },
      Arcana: { bonus: 0, proficiency: false },
      Athletics: { bonus: 0, proficiency: false },
      Deception: { bonus: 0, proficiency: false },
      History: { bonus: 0, proficiency: false },
      Insight: { bonus: 0, proficiency: false },
      Intimidation: { bonus: 0, proficiency: false },
      Investigation: { bonus: 0, proficiency: false },
      Medicine: { bonus: 0, proficiency: false },
      Nature: { bonus: 0, proficiency: false },
      Perception: { bonus: 0, proficiency: false },
      Performance: { bonus: 0, proficiency: false },
      Persuasion: { bonus: 0, proficiency: false },
      Religion: { bonus: 0, proficiency: false },
      SleightOfHand: { bonus: 0, proficiency: false },
      Stealth: { bonus: 0, proficiency: false },
      Survival: { bonus: 0, proficiency: false }
    }

    this.armorClass = 10
    this.currentHitPoints = 0
    this.deathSaveFailures = 0
    this.deathSaveSuccesses = 0
    this.initiative = 0
    this.hitPointMaximum = 0
    this.remainingHitDice = {}
    this.speed = 5
    this.temporaryHitPoints = 0
    this.totalHitDice = {}

    this.bonds = ''
    this.equipment = ''
    this.features = ''
    this.flaws = ''
    this.ideals = ''
    this.otherProficiencies = ''
    this.personality = ''
    this.languageIds = []
    this.spellcasting = ''
  }

  static from (json) {
    const characterSheet = new CharacterSheet()
    characterSheet.id = json.id
    characterSheet.editable = json.editable
    characterSheet.owned = json.owned
    characterSheet.world = json.world

    characterSheet.abilities = json.abilities
    characterSheet.alignment = json.alignment
    characterSheet.attacks = json.attacks
    characterSheet.background = json.background
    characterSheet.classes = json.classes
    characterSheet.experience = json.experience
    characterSheet.inspiration = json.inspiration
    characterSheet.name = json.name
    characterSheet.passiveWisdom = json.passiveWisdom
    characterSheet.player = json.player
    characterSheet.proficiencyBonus = json.proficiencyBonus
    characterSheet.race = json.race
    characterSheet.savingThrows = json.savingThrows
    characterSheet.skills = json.skills

    characterSheet.armorClass = json.armorClass
    characterSheet.currentHitPoints = json.currentHitPoints
    characterSheet.deathSaveFailures = json.deathSaveFailures
    characterSheet.deathSaveSuccesses = json.deathSaveSuccesses
    characterSheet.initiative = json.initiative
    characterSheet.hitPointMaximum = json.hitPointMaximum
    characterSheet.remainingHitDice = json.remainingHitDice
    characterSheet.speed = json.speed
    characterSheet.temporaryHitPoints = json.temporaryHitPoints
    characterSheet.totalHitDice = json.totalHitDice

    characterSheet.bonds = json.bonds
    characterSheet.equipment = json.equipment
    characterSheet.features = json.features
    characterSheet.flaws = json.flaws
    characterSheet.ideals = json.ideals
    characterSheet.otherProficiencies = json.otherProficiencies
    characterSheet.personality = json.personality
    characterSheet.languages = json.languages
    characterSheet.spellcasting = json.spellcasting

    return characterSheet
  }
}
