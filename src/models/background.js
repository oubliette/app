export default class Background {
  constructor () {
    this.id = null

    this.name = ''
    this.description = ''
    this.skillProficiencies = []
    this.extraLanguages = 0
    this.toolProficiencies = ''
    this.equipment = ''
    this.specialty = null
    this.feature = null

    this.suggestedCharacteristics = ''
    this.personalityTraits = []
    this.ideals = []
    this.bonds = []
    this.flaws = []
  }

  static from (json) {
    const background = new Background()
    background.id = json.id
    background.createdAt = json.createdAt
    background.updatedAt = json.updatedAt

    background.name = json.name
    background.description = json.description
    background.skillProficiencies = json.skillProficiencies
    background.extraLanguages = json.extraLanguages
    background.toolProficiencies = json.toolProficiencies
    background.equipment = json.equipment
    background.specialty = json.specialty
    background.feature = json.feature

    background.suggestedCharacteristics = json.suggestedCharacteristics
    background.personalityTraits = json.personalityTraits
    background.ideals = json.ideals
    background.bonds = json.bonds
    background.flaws = json.flaws

    return background
  }

  static toJSON (background) {
    return {
      id: background.id,
      name: background.name,
      description: background.description,
      skillProficiencies: background.skillProficiencies,
      extraLanguages: background.extraLanguages,
      toolProficiencies: background.toolProficiencies,
      equipment: background.equipment,
      specialty: background.specialty,
      feature: background.feature,
      suggestedCharacteristics: background.suggestedCharacteristics,
      personalityTraits: background.personalityTraits,
      ideals: background.ideals,
      bonds: background.bonds,
      flaws: background.flaws
    }
  }
}
