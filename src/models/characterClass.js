export default class CharacterClass {
  constructor () {
    this.classId = null
    this.level = 1
    this.primary = false
    this.remainingHitDice = 1
  }

  static from (json) {
    const character = new CharacterClass()
    character.classId = json.classId
    character.level = json.level
    character.primary = json.primary
    character.remainingHitDice = json.remainingHitDice

    return character
  }

  static toJSON (character) {
    return {
      classId: character.classId,
      level: character.level,
      primary: character.primary,
      remainingHitDice: character.remainingHitDice
    }
  }
}
