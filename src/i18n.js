import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from '@/locales/en.json'
import fr from '@/locales/fr.json'
import enCA from 'date-fns/locale/en-CA'
import frCA from 'date-fns/locale/fr-CA'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: import.meta.env.VITE_APP_I18N_LOCALE || 'en',
  fallbackLocale: import.meta.env.VITE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: {
    en,
    fr
  }
})

export const getDateLocale = () => i18n.locale === 'fr' ? frCA : enCA

export default i18n
