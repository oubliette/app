export const uppercaseChar = /[A-Z\u00C0-\u00DC]/
export const lowercaseChar = /[a-z\u00E0-\u00FC]/
export const numberChar = /[0-9]/
export const specialChar = /[\W]{1,}/
