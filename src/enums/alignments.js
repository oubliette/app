import Enum from './enum'

export default new Enum({
  chaoticEvil: 'ChaoticEvil',
  chaoticGood: 'ChaoticGood',
  chaoticNeutral: 'ChaoticNeutral',
  lawfulEvil: 'LawfulEvil',
  lawfulGood: 'LawfulGood',
  lawfulNeutral: 'LawfulNeutral',
  neutralEvil: 'NeutralEvil',
  neutralGood: 'NeutralGood',
  trueNeutral: 'TrueNeutral'
})
