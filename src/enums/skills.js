import Enum from './enum'

export default new Enum({
  acrobatics: 'Acrobatics',
  animalHandling: 'AnimalHandling',
  arcana: 'Arcana',
  athletics: 'Athletics',
  deception: 'Deception',
  history: 'History',
  insight: 'Insight',
  intimidation: 'Intimidation',
  investigation: 'Investigation',
  medicine: 'Medicine',
  nature: 'Nature',
  perception: 'Perception',
  performance: 'Performance',
  persuasion: 'Persuasion',
  religion: 'Religion',
  sleightOfHand: 'SleightOfHand',
  stealth: 'Stealth',
  survival: 'Survival'
})
