import Enum from './enum'

export default new Enum({
  save: 'Save',
  skill: 'Skill'
})
