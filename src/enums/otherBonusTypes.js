import Enum from './enum'

export default new Enum({
  armorClass: 'ArmorClass',
  initiative: 'Initiative',
  speed: 'Speed',
  hitPoints: 'HitPoints',
  passiveWisdom: 'PassiveWisdom'
})
