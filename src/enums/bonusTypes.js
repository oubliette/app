import Enum from './enum'

export default new Enum({
  ability: 'Ability',
  other: 'Other',
  save: 'Save',
  skill: 'Skill'
})
