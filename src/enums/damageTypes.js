import Enum from './enum'

export default new Enum({
  bludgeoning: 'Bludgeoning',
  piercing: 'Piercing',
  slashing: 'Slashing'
})
