import Enum from './enum'

export default new Enum({
  private: 'Private',
  protected: 'Protected',
  public: 'Public'
})
