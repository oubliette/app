import Enum from './enum'

export default new Enum({
  half: 'Half',
  proficiency: 'Proficiency',
  expertise: 'Expertise'
})
