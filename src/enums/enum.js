export default class Enum {
  constructor (values) {
    Object.assign(this, values)
  }

  getName (search) {
    return Object.entries(this).find(([_, value]) => search === value)?.[0] ?? 'none'
  }
}
