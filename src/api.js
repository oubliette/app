import router from '@/router'
import store from '@/store'
import { actions, moduleName } from '@/store/global/names'
import Vue from 'vue'
import i18n from '@/i18n'

const headers = {
  'Content-Type': 'application/json'
}

async function addAuthorizationHeader (params) {
  if (store.state[moduleName].token) {
    const { accessToken } = store.state[moduleName].token
    const { selectedWorld, temporaryWorld } = store.state[moduleName]

    if (accessToken) {
      params.headers = {
        ...params.headers,
        authorization: `Bearer ${accessToken}`
      }
    }

    if (selectedWorld) {
      params.headers = {
        ...params.headers,
        world: selectedWorld.alias
      }
    }

    if (temporaryWorld) {
      if (router.currentRoute.name === 'Edit Character') {
        params.headers = {
          ...params.headers,
          world: temporaryWorld.alias
        }
      } else {
        await store.dispatch(`${moduleName}/${actions.selectTemporaryWorld}`)
      }
    }
  }

  return params
}

async function doFetchWithRetry (uri, params, ignoreNotFound = false) {
  params = await addAuthorizationHeader(params)
  let result = await doFetch(uri, params, true, ignoreNotFound)

  if (result.status === 401) {
    if (!params.headers.authorization) {
      Vue.toasted.show(i18n.t('errors.missing_auth'))
      return result
    }

    await store.dispatch(`${moduleName}/${actions.renew}`)
    params = await addAuthorizationHeader(params)
    result = await doFetch(uri, params, false, ignoreNotFound)

    if (!params.headers.authorization) {
      Vue.toasted.error(i18n.t('errors.invalid_auth'))
    }
  }

  return result
}

async function doFetch (uri, params, ignoreForbidden = false, ignoreNotFound = false) {
  try {
    const result = await fetch(uri, params)

    if (result.status === 404) {
      if (!ignoreNotFound) {
        router.replace({ name: 'Not Found' })
      }

      return result
    }

    if (result.status === 401 && ignoreForbidden) {
      return result
    }

    if (result.status < 200 || result.status > 299) {
      const response = await result.json()
      const code = response.code || 'unexpected_error'
      Vue.toasted.error(i18n.t(`errors.${code}`))
      throw new Error(code)
    }

    return result
  } catch (error) {
    Vue.toasted.error(i18n.t('errors.unexpected_error'))
    throw error
  }
}

export const method = Object.freeze({
  get: 'GET',
  post: 'POST',
  put: 'PUT',
  delete: 'DELETE',
  patch: 'PATCH'
})

export default {
  get: (uri, ignore404 = false) => doFetchWithRetry(uri, {
    method: method.get,
    headers
  }, ignore404),
  post: (uri, data, withRetry = true) => withRetry
    ? doFetchWithRetry(uri, {
      method: method.post,
      headers,
      body: JSON.stringify(data)
    })
    : doFetch(uri, {
      method: method.post,
      headers,
      body: JSON.stringify(data)
    }),
  put: (uri, data) => doFetchWithRetry(uri, {
    method: method.put,
    headers,
    body: JSON.stringify(data)
  }),
  delete: uri => doFetchWithRetry(uri, {
    method: method.delete,
    headers
  }),
  patch: (uri, data) => doFetchWithRetry(uri, {
    method: method.patch,
    headers,
    body: JSON.stringify(data)
  })
}
