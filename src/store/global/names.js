export const moduleName = 'global'

export const getters = Object.freeze({
  isAuthenticated: 'isAuthenticated'
})

export const state = Object.freeze({
  saving: 'saving',
  theme: 'theme',
  language: 'language',

  token: 'token',

  selectedWorld: 'selectedWorld',
  temporaryWorld: 'temporaryWorld',
  world: 'world',
  worlds: 'worlds',
  worldExists: 'worldExists'
})

export const mutations = Object.freeze({
  setSaving: 'setSaving',
  setTheme: 'setTheme',
  setLanguage: 'setLanguage',

  setToken: 'setToken',

  setSelectedWorld: 'setSelectedWorld',
  setTemporaryWorld: 'setTemporaryWorld',
  setWorld: 'setWorld',
  setWorldList: 'setWorldList',
  setWorldExists: 'setWorldExists'
})

export const actions = Object.freeze({
  toggleTheme: 'toggleTheme',
  toggleLanguage: 'toggleLanguage',

  login: 'login',
  logout: 'logout',
  register: 'register',
  changePassword: 'changePassword',
  confirm: 'confirm',
  renew: 'renew',
  resetPassword: 'resetPassword',
  recoverPassword: 'recoverPassword',

  getWorld: 'getWorld',
  saveWorld: 'saveWorld',
  deleteWorld: 'deleteWorld',
  getWorldList: 'getWorldList',
  selectWorld: 'selectWorld',
  selectTemporaryWorld: 'selectTemporaryWorld',
  checkIfWorldExists: 'checkIfWorldExists'
})
