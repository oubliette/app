import { mutations, actions } from '../names'
import api from '@/api'
import i18n from '@/i18n'
import Vue from 'vue'

export default {
  async [actions.login] ({ commit }, payload) {
    commit(mutations.setToken, null)
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/login`
    const { username, password, remember } = payload
    const result = await api.post(uri, { username, password }, /* withRetry */ false)
    const { access_token: accessToken, refresh_token: refreshToken } = await result.json()
    commit(mutations.setToken, remember
      ? { accessToken, refreshToken }
      : { accessToken })
    Vue.toasted.success(i18n.t('global.loggedIn'))
  },
  async [actions.renew] ({ state, commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/renew`
    const { token } = state
    commit(mutations.setToken, null)

    if (token && token.refreshToken) {
      const result = await api.post(uri, { refresh_token: token.refreshToken }, /* withRetry */ false)
      const { access_token: accessToken, refresh_token: refreshToken } = await result.json()
      return commit(mutations.setToken, { accessToken, refreshToken })
    }
  },
  async [actions.register] (_, payload) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/register`
    await api.post(uri, payload)
  },
  async [actions.confirm] (_, payload) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/confirm`
    await api.post(uri, payload)
    Vue.toasted.success(i18n.t('global.confirmationSuccessful'))
  },
  async [actions.recoverPassword] (_, payload) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/password/recover`
    await api.post(uri, payload)
  },
  async [actions.resetPassword] (_, payload) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/password/reset`
    await api.post(uri, payload)
    Vue.toasted.success(i18n.t('global.passwordReset'))
  },
  async [actions.changePassword] (_, payload) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/user/password/change`
    await api.post(uri, payload)
    Vue.toasted.success(i18n.t('global.passwordChanged'))
  },
  async [actions.logout] ({ commit, state }) {
    if (state.token && state.token.refreshToken) {
      try {
        const uri = `${import.meta.env.VITE_APP_BASE_API}/user/logout`
        await api.post(uri, { refresh_token: state.token.refreshToken })
      } catch (error) {}
    }

    commit(mutations.setToken, null)
    commit(mutations.setSelectedWorld, null)
    Vue.toasted.show(i18n.t('global.loggedOut'))
  }
}
