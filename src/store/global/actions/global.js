import { actions, mutations } from '../names'

export default {
  [actions.toggleTheme] ({ commit }, payload) {
    commit(mutations.setTheme, payload)
  },
  [actions.toggleLanguage] ({ commit }, payload) {
    commit(mutations.setLanguage, payload)
  }
}
