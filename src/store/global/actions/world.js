import { actions, mutations } from '../names'
import api from '@/api'
import World from '@/models/world'
import Vue from 'vue'
import i18n from '@/i18n'
import WorldSummary from '@/models/worldSummary'

export default {
  async [actions.getWorld] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/worlds/${payload}`
      const response = await api.get(uri, true)

      if (response.status === 404) {
        commit(mutations.setWorld, null)
      } else {
        const world = await response.json()
        commit(mutations.setWorld, World.from(world))
      }
    } else {
      const world = new World()
      commit(mutations.setWorld, world)
    }
  },
  async [actions.checkIfWorldExists] ({ commit }, payload) {
    if (payload.alias === payload.initialAlias) {
      commit(mutations.setWorldExists, false)
    } else {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/worlds/${payload.alias}`

      try {
        const result = await api.get(uri, true)
        commit(mutations.setWorldExists, result.status !== 404)
      } catch (error) {
        commit(mutations.setWorldExists, true)
      }
    }
  },
  async [actions.saveWorld] ({ commit, state }) {
    let response
    if (state.world) {
      try {
        commit(mutations.setSaving, true)
        let world = World.toJSON(state.world)

        if (world.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/worlds/${state.world.id}`
          response = await api.put(uri, world)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/worlds`
          response = await api.post(uri, world)
        }

        world = await response.json()
        commit(mutations.setWorld, World.from(world))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },

  async [actions.deleteWorld] ({ dispatch, state }) {
    if (state.world && state.world.id) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/worlds/${state.world.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    await dispatch(actions.getWorldList)

    if (state.worlds.length) {
      await dispatch(actions.selectWorld, state.worlds[0].alias)
    } else {
      await dispatch(actions.selectWorld, null)
    }
  },
  async [actions.getWorldList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/worlds`
    const response = await api.get(uri, true)

    if (response.status === 404) {
      commit(mutations.setWorldList, [])
      commit(mutations.setSelectedWorld, null)
    } else {
      const data = await response.json()
      const worlds = data.map(value => new WorldSummary(value))
      commit(mutations.setWorldList, worlds)
    }
  },
  async [actions.selectWorld] ({ commit, dispatch, state }, payload) {
    if (payload) {
      await dispatch(actions.getWorld, payload)
      const world = state.world
      commit(mutations.setSelectedWorld, world)
    } else {
      commit(mutations.setSelectedWorld, null)
    }
  },
  async [actions.selectTemporaryWorld] ({ commit, dispatch, state }, payload) {
    if (payload) {
      await dispatch(actions.getWorld, payload)
      const world = state.world
      commit(mutations.setTemporaryWorld, world)
    } else {
      commit(mutations.setTemporaryWorld, null)
    }
  }
}
