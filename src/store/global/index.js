import { state, mutations, getters } from './names'
import { mutate } from '@/store/utils'
import global from './actions/global'
import account from './actions/account'
import world from './actions/world'

export default {
  state: {
    [state.saving]: false,
    [state.theme]: null,
    [state.language]: 'en',

    [state.token]: null,

    [state.selectedWorld]: null,
    [state.temporaryWorld]: null,
    [state.world]: null,
    [state.worlds]: [],
    [state.worldExists]: false
  },
  mutations: {
    [mutations.setSaving]: mutate(state.saving),
    [mutations.setTheme]: mutate(state.theme),
    [mutations.setLanguage]: mutate(state.language),

    [mutations.setToken]: mutate(state.token),

    [mutations.setSelectedWorld]: mutate(state.selectedWorld),
    [mutations.setTemporaryWorld]: mutate(state.temporaryWorld),
    [mutations.setWorld]: mutate(state.world),
    [mutations.setWorldList]: mutate(state.worlds),
    [mutations.setWorldExists]: mutate(state.worldExists)
  },
  actions: {
    ...global,
    ...account,
    ...world
  },
  getters: {
    [getters.isAuthenticated] (state) {
      if (!import.meta.env.VITE_APP_AUTHENTICATION) {
        return true
      }

      return state.token !== null
    }
  },
  namespaced: true
}
