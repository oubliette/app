import game from './game'
import { moduleName as gameModuleName } from './game/names'
import global from './global'
import { moduleName as globalModuleName } from './global/names'
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: [
    globalModuleName
  ]
})

const store = new Vuex.Store({
  modules: {
    [gameModuleName]: game,
    [globalModuleName]: global
  },
  plugins: [vuexLocal.plugin]
})

export default store
