import { actions, mutations } from '../names'
import api from '@/api'
import CharacterClass from '@/models/characterClass'

export default {
  async [actions.saveCharacterClass] ({ state, commit }, payload) {
    if (state.character && payload?.classId) {
      try {
        commit(mutations.setSaving, true)
        const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${state.character.id}/classes/${payload.classId}`
        await api.put(uri, CharacterClass.toJSON(payload))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deleteCharacterClass] ({ state }, payload) {
    if (state.character && payload?.classId) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${state.character.id}/classes/${payload.classId}`
      await api.delete(uri)
    }
  },
  async [actions.importCharacterClassList] ({ commit }, payload) {
    const newCharacterClassList = [...payload]
    commit(mutations.setCharacterClassList, newCharacterClassList)
  },
  async [actions.getCharacterClassList] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${payload}/classes`
      const response = await api.get(uri)
      const data = await response.json()
      const characterClasses = data.map(value => CharacterClass.from(value))
      const sortedCharacterClasses = [...characterClasses].sort((a, b) => b.primary - a.primary)
      commit(mutations.setCharacterClassList, sortedCharacterClasses)
    } else {
      commit(mutations.setCharacterClassList, [])
    }
  }
}
