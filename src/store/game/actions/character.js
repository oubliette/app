import { actions, mutations } from '../names'
import { applyFilter } from '../../utils'
import api from '@/api'
import Character from '@/models/character'
import CharacterSheet from '@/models/characterSheet'
import CharacterSummary from '@/models/characterSummary'
import Vue from 'vue'
import i18n from '@/i18n'

export default {
  async [actions.getCharacterSheet] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/sheets/${payload}`
      const response = await api.get(uri)
      const character = await response.json()
      commit(mutations.setCharacterSheet, CharacterSheet.from(character))
    } else {
      const character = new CharacterSheet()
      commit(mutations.setCharacter, character)
    }
  },
  async [actions.getCharacter] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${payload}`
      const response = await api.get(uri)
      const character = await response.json()
      commit(mutations.setCharacter, Character.from(character))
    } else {
      const character = new Character()
      commit(mutations.setCharacter, character)
    }
  },
  async [actions.saveCharacter] ({ commit, state }) {
    let response

    if (state.character) {
      try {
        commit(mutations.setSaving, true)
        let character = Character.toJSON(state.character)

        if (character.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${state.character.id}`
          response = await api.put(uri, character)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/characters`
          response = await api.post(uri, character)
        }

        character = await response.json()
        commit(mutations.setCharacter, Character.from(character))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deleteCharacter] ({ dispatch, state }, payload) {
    if (payload || (state.character && state.character.id)) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${payload || state.character.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    if (payload) {
      dispatch(actions.getCharacterList)
    }
  },
  async [actions.importCharacter] ({ commit, state }, payload) {
    const character = state.character
    const newCharacter = { ...character, ...payload }
    commit(mutations.setCharacter, newCharacter)
  },
  async [actions.getPlayerList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/players`
    const response = await api.get(uri)
    const data = await response.json()
    commit(mutations.setPlayerList, data)
  },
  async [actions.getSimpleCharacterList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/characters?sort=Name`
    const response = await api.get(uri)
    const data = await response.json()
    const characters = data.items.map(value => new CharacterSummary(value))
    commit(mutations.setCharacterList, characters)
  },
  async [actions.getCharacterList] ({ commit, state }) {
    let uri = `${import.meta.env.VITE_APP_BASE_API}/characters`
    uri = applyFilter(state, uri)
    const response = await api.get(uri)
    const data = await response.json()
    const characters = data.items.map(value => new CharacterSummary(value))
    commit(mutations.setCharacterList, characters)
  },
  async [actions.getSharedCharacterList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/sheets`
    const response = await api.get(uri)
    const data = await response.json()
    const characters = data.map(value => new CharacterSummary(value))
    commit(mutations.setSharedCharacterList, characters)
  },
  async [actions.duplicateCharacter] ({ dispatch }, payload) {
    if (payload) {
      let uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${payload}`
      let response = await api.get(uri)
      const character = await response.json()
      character.id = null
      character.name += i18n.t('global.copy')
      uri = `${import.meta.env.VITE_APP_BASE_API}/characters`
      response = await api.post(uri, character)
      await response.json()
      Vue.toasted.success(i18n.t('global.success'))
      dispatch(actions.getCharacterList)
    }
  }
}
