import { actions, mutations } from '../names'
import { applyFilter } from '../../utils'
import api from '@/api'
import Language from '@/models/language'
import Vue from 'vue'
import i18n from '@/i18n'
import LanguageSummary from '@/models/languageSummary'

export default {
  async [actions.getLanguage] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/languages/${payload}`
      const response = await api.get(uri)
      const language = await response.json()
      commit(mutations.setLanguage, Language.from(language))
    } else {
      const language = new Language()
      commit(mutations.setLanguage, language)
    }
  },
  async [actions.saveLanguage] ({ commit, state }) {
    let response
    if (state.language) {
      try {
        commit(mutations.setSaving, true)
        let language = Language.toJSON(state.language)

        if (language.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/languages/${state.language.id}`
          response = await api.put(uri, language)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/languages`
          response = await api.post(uri, language)
        }

        language = await response.json()
        commit(mutations.setLanguage, Language.from(language))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },

  async [actions.deleteLanguage] ({ dispatch, state }, payload) {
    if (payload || (state.language && state.language.id)) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/languages/${payload || state.language.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    if (payload) {
      dispatch(actions.getLanguageList)
    }
  },
  async [actions.importLanguage] ({ commit, state }, payload) {
    const language = state.language
    const newLanguage = { ...language, ...payload }
    commit(mutations.setLanguage, newLanguage)
  },
  async [actions.getSimpleLanguageList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/languages?sort=Name`
    const response = await api.get(uri)
    const data = await response.json()
    const languages = data.items.map(value => new LanguageSummary(value))
    commit(mutations.setLanguageList, languages)
  },
  async [actions.getLanguageList] ({ commit, state }) {
    let uri = `${import.meta.env.VITE_APP_BASE_API}/languages/`
    uri = applyFilter(state, uri)
    const response = await api.get(uri)
    const data = await response.json()
    const languages = data.items.map(value => new LanguageSummary(value))
    commit(mutations.setLanguageList, languages)
  },
  async [actions.duplicateLanguage] ({ dispatch }, payload) {
    if (payload) {
      let uri = `${import.meta.env.VITE_APP_BASE_API}/languages/${payload}`
      let response = await api.get(uri)
      const language = await response.json()
      language.id = null
      language.name += i18n.t('global.copy')
      uri = `${import.meta.env.VITE_APP_BASE_API}/languages`
      response = await api.post(uri, language)
      await response.json()
      Vue.toasted.success(i18n.t('global.success'))
      dispatch(actions.getLanguageList)
    }
  },
  async [actions.getLanguageScriptList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/languages/scripts`
    const response = await api.get(uri)
    const scripts = await response.json()
    commit(mutations.setLanguageScriptList, scripts)
  }
}
