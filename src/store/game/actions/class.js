import { actions, mutations } from '../names'
import { applyFilter } from '../../utils'
import api from '@/api'
import Class from '@/models/class'
import ClassSummary from '@/models/classSummary'
import Vue from 'vue'
import i18n from '@/i18n'

export default {
  async [actions.getClass] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/classes/${payload}`
      const response = await api.get(uri)
      const _class = await response.json()
      commit(mutations.setClass, Class.from(_class))
    } else {
      const _class = new Class()
      commit(mutations.setClass, _class)
    }
  },
  async [actions.createClass] ({ commit }, payload) {
    if (payload) {
      try {
        commit(mutations.setSaving, true)
        let _class = new Class()
        _class.name = payload
        _class = Class.toJSON(_class)
        const uri = `${import.meta.env.VITE_APP_BASE_API}/classes`
        const response = await api.post(uri, _class)
        _class = await response.json()
        commit(mutations.setClass, _class)
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.saveClass] ({ commit, state }) {
    let response

    if (state._class) {
      try {
        commit(mutations.setSaving, true)
        let _class = Class.toJSON(state._class)

        if (_class.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/classes/${_class.id}`
          response = await api.put(uri, _class)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/classes`
          response = await api.post(uri, _class)
        }

        _class = await response.json()
        commit(mutations.setClass, Class.from(_class))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deleteClass] ({ dispatch, state }, payload) {
    if (payload || (state._class && state._class.id)) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/classes/${payload || state._class.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    if (payload) {
      dispatch(actions.getClassList)
    }
  },
  async [actions.importClass] ({ commit, state }, payload) {
    const _class = state._class
    const newClass = { ..._class, ...payload }
    commit(mutations.setClass, newClass)
  },
  async [actions.getSimpleClassList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/classes?sort=Name`
    const response = await api.get(uri)
    const data = await response.json()
    const classes = data.items.map(value => new ClassSummary(value))
    commit(mutations.setClassList, classes)
  },
  async [actions.getClassList] ({ commit, state }) {
    let uri = `${import.meta.env.VITE_APP_BASE_API}/classes`
    uri = applyFilter(state, uri)
    const response = await api.get(uri)
    const data = await response.json()
    const classes = data.items.map(value => new ClassSummary(value))
    commit(mutations.setClassList, classes)
  },
  async [actions.duplicateClass] ({ dispatch }, payload) {
    if (payload) {
      let uri = `${import.meta.env.VITE_APP_BASE_API}/classes/${payload}`
      let response = await api.get(uri)
      const _class = await response.json()
      _class.id = null
      _class.name += i18n.t('global.copy')
      uri = `${import.meta.env.VITE_APP_BASE_API}/classes`
      response = await api.post(uri, _class)
      await response.json()
      Vue.toasted.success(i18n.t('global.success'))
      dispatch(actions.getClassList)
    }
  }
}
