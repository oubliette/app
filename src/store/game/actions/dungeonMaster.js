import { actions, mutations } from '../names'
import api from '@/api'
import CharacterSheet from '@/models/characterSheet'
import { asyncForEach } from '@/utils'

export default {
  async [actions.getDungeonMasterCharacterList] ({ commit, state }) {
    if (state.party?.id) {
      let characterIds

      {
        const uri = `${import.meta.env.VITE_APP_BASE_API}/characters?sort=Name&partyId=${state.party.id}`
        const response = await api.get(uri)
        const data = await response.json()
        characterIds = data.items.map(value => value.id)
      }

      const characters = []
      await asyncForEach(characterIds, async id => {
        const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/sheets/${id}`
        const response = await api.get(uri)
        const data = await response.json()
        characters.push(CharacterSheet.from(data))
      })
      commit(mutations.setDungeonMasterCharacterList, characters)
    } else {
      commit(mutations.setDungeonMasterCharacterList, [])
    }
  }
}
