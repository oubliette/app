import { actions, mutations } from '../names'
import { applyFilter } from '../../utils'
import api from '@/api'
import Background from '@/models/background'
import BackgroundSummary from '@/models/backgroundSummary'
import Vue from 'vue'
import i18n from '@/i18n'

export default {
  async [actions.getBackground] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds/${payload}`
      const response = await api.get(uri)
      const background = await response.json()
      commit(mutations.setBackground, Background.from(background))
    } else {
      const background = new Background()
      commit(mutations.setBackground, background)
    }
  },
  async [actions.createBackground] ({ commit }, payload) {
    if (payload) {
      try {
        commit(mutations.setSaving, true)
        let background = new Background()
        background.name = payload
        background = Background.toJSON(background)
        const uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds`
        const response = await api.post(uri, background)
        background = await response.json()
        commit(mutations.setBackground, background)
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.saveBackground] ({ commit, state }) {
    let response

    if (state.background) {
      try {
        commit(mutations.setSaving, true)
        let background = Background.toJSON(state.background)

        if (background.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds/${background.id}`
          response = await api.put(uri, background)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds`
          response = await api.post(uri, background)
        }

        background = await response.json()
        commit(mutations.setBackground, Background.from(background))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deleteBackground] ({ dispatch, state }, payload) {
    if (payload || (state.background && state.background.id)) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds/${payload || state.background.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    if (payload) {
      dispatch(actions.getBackgroundList)
    }
  },
  async [actions.importBackground] ({ commit, state }, payload) {
    const background = state.background
    const newBackground = { ...background, ...payload }
    commit(mutations.setBackground, newBackground)
  },
  async [actions.getSimpleBackgroundList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds?sort=Name`
    const response = await api.get(uri)
    const data = await response.json()
    const backgrounds = data.items.map(value => new BackgroundSummary(value))
    commit(mutations.setBackgroundList, backgrounds)
  },
  async [actions.getBackgroundList] ({ commit, state }) {
    let uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds`
    uri = applyFilter(state, uri)
    const response = await api.get(uri)
    const data = await response.json()
    const backgrounds = data.items.map(value => new BackgroundSummary(value))
    commit(mutations.setBackgroundList, backgrounds)
  },
  async [actions.duplicateBackground] ({ dispatch }, payload) {
    if (payload) {
      let uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds/${payload}`
      let response = await api.get(uri)
      const background = await response.json()
      background.id = null
      background.name += i18n.t('global.copy')
      uri = `${import.meta.env.VITE_APP_BASE_API}/backgrounds`
      response = await api.post(uri, background)
      await response.json()
      Vue.toasted.success(i18n.t('global.success'))
      dispatch(actions.getBackgroundList)
    }
  }
}
