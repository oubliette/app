import { mutations, actions } from '../names'
import api from '@/api'
import Vue from 'vue'

export default {
  async [actions.createInvitation] ({ commit }, payload) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/invitation/create`
    const result = await api.post(uri, { characterId: payload })
    const invitation = await result.json()
    commit(mutations.setInvitation, invitation)
  },
  async [actions.resolveInvitation] (_, payload) {
    const { id, token } = payload
    const uri = `${import.meta.env.VITE_APP_BASE_API}/invitation/resolve`
    await api.post(uri, { id, token })
    Vue.toasted.success('Invitation confirmed!')
  }
}
