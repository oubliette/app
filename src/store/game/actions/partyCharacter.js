import { actions, mutations } from '../names'
import api from '@/api'
import CharacterSummary from '@/models/characterSummary'

export default {
  async [actions.savePartyCharacter] ({ state, commit }, payload) {
    if (state.party && payload?.id) {
      try {
        commit(mutations.setSaving, true)
        const uri = `${import.meta.env.VITE_APP_BASE_API}/parties/${state.party.id}/characters/${payload.id}`
        await api.post(uri)
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deletePartyCharacter] ({ dispatch, state }, payload) {
    if (state.party && payload?.id) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/parties/${state.party.id}/characters/${payload.id}`
      await api.delete(uri)
      dispatch(actions.getPartyCharacterList)
    }
  },
  async [actions.getPartyCharacterList] ({ commit, state }) {
    if (state.party?.id) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters?sort=Name&partyId=${state.party.id}`
      const response = await api.get(uri)
      const data = await response.json()
      const characters = data.items.map(value => new CharacterSummary(value))
      commit(mutations.setPartyCharacterList, characters)
    } else {
      commit(mutations.setPartyCharacterList, [])
    }
  }
}
