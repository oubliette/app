import { actions, mutations } from '../names'
import { applyFilter } from '../../utils'
import api from '@/api'
import Race from '@/models/race'
import RaceSummary from '@/models/raceSummary'
import Vue from 'vue'
import i18n from '@/i18n'

export default {
  async [actions.getRace] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/races/${payload}`
      const response = await api.get(uri)
      const race = await response.json()
      commit(mutations.setRace, Race.from(race))
    } else {
      const race = new Race()
      commit(mutations.setRace, race)
    }
  },
  async [actions.createRace] ({ commit }, payload) {
    if (payload) {
      try {
        commit(mutations.setSaving, true)
        let race = new Race()
        race.name = payload
        race = Race.toJSON(race)
        const uri = `${import.meta.env.VITE_APP_BASE_API}/races`
        const response = await api.post(uri, race)
        race = await response.json()
        commit(mutations.setRace, race)
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.saveRace] ({ commit, state }) {
    let response

    if (state.race) {
      try {
        commit(mutations.setSaving, true)
        let race = Race.toJSON(state.race)

        if (race.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/races/${race.id}`
          response = await api.put(uri, race)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/races`
          response = await api.post(uri, race)
        }

        race = await response.json()
        commit(mutations.setRace, Race.from(race))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deleteRace] ({ dispatch, state }, payload) {
    if (payload || (state.race && state.race.id)) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/races/${payload || state.race.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    if (payload) {
      dispatch(actions.getRaceList)
    }
  },
  async [actions.importRace] ({ commit, state }, payload) {
    const race = state.race
    const newRace = { ...race, ...payload }
    commit(mutations.setRace, newRace)
  },
  async [actions.getSimpleRaceList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/races?sort=Name`
    const response = await api.get(uri)
    const data = await response.json()
    const races = data.items.map(value => new RaceSummary(value))
    commit(mutations.setRaceList, races)
  },
  async [actions.getRaceList] ({ commit, state }) {
    let uri = `${import.meta.env.VITE_APP_BASE_API}/races`
    uri = applyFilter(state, uri)
    const response = await api.get(uri)
    const data = await response.json()
    const races = data.items.map(value => new RaceSummary(value))
    commit(mutations.setRaceList, races)
  },
  async [actions.duplicateRace] ({ dispatch }, payload) {
    if (payload) {
      let uri = `${import.meta.env.VITE_APP_BASE_API}/races/${payload}`
      let response = await api.get(uri)
      const race = await response.json()
      race.id = null
      race.name += i18n.t('global.copy')
      uri = `${import.meta.env.VITE_APP_BASE_API}/races`
      response = await api.post(uri, race)
      await response.json()
      Vue.toasted.success(i18n.t('global.success'))
      dispatch(actions.getRaceList)
    }
  }
}
