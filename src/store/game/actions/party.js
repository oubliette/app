import { actions, mutations } from '../names'
import api from '@/api'
import Party from '@/models/party'
import PartySummary from '@/models/partySummary'
import Vue from 'vue'
import i18n from '@/i18n'

export default {
  async [actions.getParty] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/parties/${payload}`
      const response = await api.get(uri)
      const party = await response.json()
      commit(mutations.setParty, Party.from(party))
    } else {
      const party = new Party()
      commit(mutations.setParty, party)
    }
  },
  async [actions.saveParty] ({ commit, state }) {
    let response

    if (state.party) {
      try {
        commit(mutations.setSaving, true)
        let party = Party.toJSON(state.party)

        if (party.id) {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/parties/${party.id}`
          response = await api.put(uri, party)
        } else {
          const uri = `${import.meta.env.VITE_APP_BASE_API}/parties`
          response = await api.post(uri, party)
        }

        party = await response.json()
        commit(mutations.setParty, Party.from(party))
        Vue.toasted.success(i18n.t('global.success'))
      } finally {
        commit(mutations.setSaving, false)
      }
    }
  },
  async [actions.deleteParty] ({ dispatch, state }, payload) {
    if (payload || (state.party && state.party.id)) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/parties/${payload || state.party.id}`
      await api.delete(uri)
      Vue.toasted.success(i18n.t('global.success'))
    }

    if (payload) {
      dispatch(actions.getPartyList)
    }
  },
  async [actions.getPartyList] ({ commit }) {
    const uri = `${import.meta.env.VITE_APP_BASE_API}/parties`
    const response = await api.get(uri)
    const data = await response.json()
    const parties = data.map(value => new PartySummary(value))
    commit(mutations.setPartyList, parties)
  },
  async [actions.getCharacterPartyList] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/parties?characterId=${payload}`
      const response = await api.get(uri)
      const data = await response.json()
      const parties = data.map(value => new PartySummary(value))
      commit(mutations.setPartyList, parties)
    }
  }
}
