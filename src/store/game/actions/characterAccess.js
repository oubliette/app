import { actions, mutations } from '../names'
import api from '@/api'
import CharacterAccess from '@/models/characterAccess'

export default {
  async [actions.deleteCharacterAccess] (_, payload) {
    if (payload?.id) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/accesses/${payload.id}`
      await api.delete(uri)
    }
  },
  async [actions.getCharacterAccessList] ({ commit }, payload) {
    if (payload) {
      const uri = `${import.meta.env.VITE_APP_BASE_API}/characters/${payload}/accesses`
      const response = await api.get(uri)
      const data = await response.json()
      const characterAccesses = data.map(value => CharacterAccess.from(value))
      const sortedCharacterAccesses = [...characterAccesses].sort((a, b) => b.primary - a.primary)
      commit(mutations.setCharacterAccessList, sortedCharacterAccesses)
    } else {
      commit(mutations.setCharacterAccessList, [])
    }
  }
}
