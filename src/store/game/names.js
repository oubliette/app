export const moduleName = 'game'

export const state = Object.freeze({
  filter: 'filter',
  saving: 'saving',
  invitation: 'invitation',

  background: 'background',
  backgrounds: 'backgrounds',

  party: 'party',
  parties: 'parties',

  partyCharacters: 'partyCharacters',

  character: 'character',
  characters: 'characters',
  players: 'players',
  sharedCharacters: 'sharedCharacters',
  characterSheet: 'characterSheet',

  characterAccesses: 'characterAccesses',

  characterClasses: 'characterClasses',

  _class: '_class',
  classes: 'classes',

  race: 'race',
  races: 'races',

  language: 'language',
  languages: 'languages',
  languageScripts: 'languageScripts',

  dungeonMasterCharacters: 'dungeonMasterCharacters'
})

export const mutations = Object.freeze({
  setFilter: 'setFilter',
  setSaving: 'setSaving',
  setInvitation: 'setInvitation',

  setBackground: 'setBackground',
  setBackgroundList: 'setBackgroundList',

  setParty: 'setParty',
  setPartyList: 'setPartyList',

  setPartyCharacterList: 'setPartyCharacterList',

  setCharacter: 'setCharacter',
  setCharacterList: 'setCharacterList',
  setPlayerList: 'setPlayerList',
  setSharedCharacterList: 'setSharedCharacterList',
  setCharacterSheet: 'setCharacterSheet',

  setCharacterAccessList: 'setCharacterAccessList',

  setCharacterClassList: 'setCharacterClassList',

  setClass: 'setClass',
  setClassList: 'setClassList',

  setRace: 'setRace',
  setRaceList: 'setRaceList',

  setLanguage: 'setLanguage',
  setLanguageList: 'setLanguageList',
  setLanguageScriptList: 'setLanguageScriptList',

  setDungeonMasterCharacterList: 'setDungeonMasterCharacterList'
})

export const actions = Object.freeze({
  createInvitation: 'createInvitation',
  resolveInvitation: 'resolveInvitation',

  getBackground: 'getBackground',
  createBackground: 'createBackground',
  saveBackground: 'saveBackground',
  deleteBackground: 'deleteBackground',
  importBackground: 'importBackground',
  getBackgroundList: 'getBackgroundList',
  duplicateBackground: 'duplicateBackground',
  getSimpleBackgroundList: 'getSimpleBackgroundList',

  getParty: 'getParty',
  createParty: 'createParty',
  saveParty: 'saveParty',
  deleteParty: 'deleteParty',
  getPartyList: 'getPartyList',
  getCharacterPartyList: 'getCharacterPartyList',

  savePartyCharacter: 'savePartyCharacter',
  deletePartyCharacter: 'deletePartyCharacter',
  getPartyCharacterList: 'getPartyCharacterList',

  getCharacter: 'getCharacter',
  getCharacterSheet: 'getCharacterSheet',
  saveCharacter: 'saveCharacter',
  deleteCharacter: 'deleteCharacter',
  importCharacter: 'importCharacter',
  getCharacterList: 'getCharacterList',
  getPlayerList: 'getPlayerList',
  getSharedCharacterList: 'getSharedCharacterList',
  duplicateCharacter: 'duplicateCharacter',
  getSimpleCharacterList: 'getSimpleCharacterList',

  deleteCharacterAccess: 'deleteCharacterAccess',
  getCharacterAccessList: 'getCharacterAccessList',

  saveCharacterClass: 'saveCharacterClass',
  deleteCharacterClass: 'deleteCharacterClass',
  importCharacterClassList: 'importCharacterClassList',
  getCharacterClassList: 'getCharacterClassList',

  getClass: 'getClass',
  createClass: 'createClass',
  saveClass: 'saveClass',
  deleteClass: 'deleteClass',
  importClass: 'importClass',
  getClassList: 'getClassList',
  duplicateClass: 'duplicateClass',
  getSimpleClassList: 'getSimpleClassList',

  getRace: 'getRace',
  createRace: 'createRace',
  saveRace: 'saveRace',
  deleteRace: 'deleteRace',
  importRace: 'importRace',
  getRaceList: 'getRaceList',
  duplicateRace: 'duplicateRace',
  getSimpleRaceList: 'getSimpleRaceList',

  getLanguage: 'getLanguage',
  saveLanguage: 'saveLanguage',
  deleteLanguage: 'deleteLanguage',
  importLanguage: 'importLanguage',
  getLanguageList: 'getLanguageList',
  duplicateLanguage: 'duplicateLanguage',
  getLanguageScriptList: 'getLanguageScriptList',
  getSimpleLanguageList: 'getSimpleLanguageList',

  getDungeonMasterCharacterList: 'getDungeonMasterCharacterList'
})
