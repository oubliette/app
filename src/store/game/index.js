import { getField, updateField } from 'vuex-map-fields'
import { state, mutations } from './names'
import { mutate } from '@/store/utils'
import background from './actions/background'
import party from './actions/party'
import partyCharacter from './actions/partyCharacter'
import character from './actions/character'
import characterAccess from './actions/characterAccess'
import characterClass from './actions/characterClass'
import _class from './actions/class'
import invitation from './actions/invitation'
import race from './actions/race'
import language from './actions/language'
import dungeonMaster from './actions/dungeonMaster'

export default {
  state: {
    [state.filter]: {},
    [state.saving]: false,
    [state.invitation]: null,

    [state.background]: null,
    [state.backgrounds]: [],

    [state.party]: null,
    [state.parties]: [],

    [state.partyCharacters]: [],

    [state.character]: null,
    [state.characters]: [],
    [state.players]: [],
    [state.sharedCharacters]: [],
    [state.characterSheet]: null,

    [state.characterAccesses]: [],

    [state.characterClasses]: [],

    [state._class]: null,
    [state.classes]: [],

    [state.race]: null,
    [state.races]: [],

    [state.language]: null,
    [state.languages]: [],
    [state.languageScripts]: [],

    [state.dungeonMasterCharacters]: []
  },
  mutations: {
    [mutations.setFilter]: mutate(state.filter),
    [mutations.setSaving]: mutate(state.saving),
    [mutations.setInvitation]: mutate(state.invitation),

    [mutations.setBackground]: mutate(state.background),
    [mutations.setBackgroundList]: mutate(state.backgrounds),

    [mutations.setParty]: mutate(state.party),
    [mutations.setPartyList]: mutate(state.parties),

    [mutations.setPartyCharacterList]: mutate(state.partyCharacters),

    [mutations.setCharacter]: mutate(state.character),
    [mutations.setCharacterList]: mutate(state.characters),
    [mutations.setPlayerList]: mutate(state.players),
    [mutations.setSharedCharacterList]: mutate(state.sharedCharacters),
    [mutations.setCharacterSheet]: mutate(state.characterSheet),

    [mutations.setCharacterAccessList]: mutate(state.characterAccesses),

    [mutations.setCharacterClassList]: mutate(state.characterClasses),

    [mutations.setClass]: mutate(state._class),
    [mutations.setClassList]: mutate(state.classes),

    [mutations.setRace]: mutate(state.race),
    [mutations.setRaceList]: mutate(state.races),

    [mutations.setLanguage]: mutate(state.language),
    [mutations.setLanguageList]: mutate(state.languages),
    [mutations.setLanguageScriptList]: mutate(state.languageScripts),

    [mutations.setDungeonMasterCharacterList]: mutate(state.dungeonMasterCharacters),

    updateField
  },
  actions: {
    ...background,
    ...party,
    ...partyCharacter,
    ...character,
    ...characterAccess,
    ...characterClass,
    ..._class,
    ...invitation,
    ...race,
    ...language,
    ...dungeonMaster
  },
  getters: {
    getField
  },
  namespaced: true
}
