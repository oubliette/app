export const mutate = field => (state, payload) => { state[field] = payload }
export const applyFilter = (state, uri) => {
  let newUri = uri
  let isFirstParam = !uri.includes('?')

  if (state.filter?.sort) {
    newUri = `${newUri}${isFirstParam ? '?' : '&'}sort=${state.filter.sort.value}&desc=${state.filter.sort.desc}`
    isFirstParam = false
  }

  for (const filter in state.filter) {
    if (state.filter[filter] !== null && filter !== 'sort') {
      newUri = `${newUri}${isFirstParam ? '?' : '&'}${filter}=${state.filter[filter]}`
      isFirstParam = false
    }
  }

  return newUri
}
