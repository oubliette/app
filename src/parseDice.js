export const parseDice = val => {
  if (!val || !~val.indexOf('d')) {
    return null
  }

  const [dice, rest] = val.split('d').map(x => x.trim())
  const matches = rest.match(/[+-]?\d+/g)

  if (matches) {
    const [die, bonus] = matches.map(x => Number(x))
    return { dice: Number(dice), die, bonus }
  }

  return { dice: Number(dice), die: Number(rest), bonus: 0 }
}

export const getDice = ({ dice, die, bonus }) => {
  if (!dice || !die) {
    return ''
  }

  return `${dice}d${die}${bonus > 0 ? '+' : ''}${bonus || ''}`
}
