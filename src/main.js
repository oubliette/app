import 'reset-css'
import 'line-awesome/dist/line-awesome/css/line-awesome.css'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import PortalVue from 'portal-vue'
import VueClipboard from 'vue-clipboard2'
import * as Sentry from '@sentry/browser'
import { Vue as VueIntegration } from '@sentry/integrations'
import VueGtag from 'vue-gtag'
import Toasted from 'vue-toasted'
import './fit-text-directive'

// eslint-disable-next-line no-console
console.log(`%cOubliette v${import.meta.env.VITE_APP_VERSION}`, 'font-size: 40px')

Sentry.init({
  dsn: import.meta.env.VITE_APP_SENTRY_DSN,
  integrations: [new VueIntegration({ Vue, attachProps: true })]
})

VueClipboard.config.autoSetContainer = true
Vue.use(VueClipboard)
Vue.use(PortalVue)
Vue.use(Toasted, {
  position: 'bottom-right',
  duration: 5000,
  keepOnHover: true
})
Vue.use(VueGtag, {
  config: { id: import.meta.env.VITE_APP_GTAG }
})
Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
