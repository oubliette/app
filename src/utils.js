export const clone = obj => JSON.parse(JSON.stringify(obj))
export const equals = (a, b) => JSON.stringify(a) === JSON.stringify(b)
export const isTrue = value => value
export const clamp = (number, min, max) => number <= min ? min : number >= max ? max : number
export const asyncForEach = async (array, callback) => { for (let index = 0; index < array.length; index++) await callback(array[index], index, array) }
export const instanceOf = (instance, klass) => instance.constructor.name === klass.name
export const objectFilter = (object, callback) => Object.entries(object).filter(callback).reduce((obj, [x, y]) => ({ ...obj, [x]: y }), {})
export const exceptDefault = (value, def = null) => Array.isArray(value) ? value.filter(x => !equals(x, def)) : objectFilter(value, ([_, y]) => !equals(y, def))
export const exceptNullOrEmpty = value => Array.isArray(value) ? value.filter(x => !!x) : objectFilter(value, ([_, y]) => !!y)
export const extractPropFromObjects = (arr, prop) => arr.map(a => a[prop])
export const arrayRemove = (array, index) => array.filter((_, i) => i !== index)
export const timeout = ms => new Promise(resolve => setTimeout(resolve, ms))
export const enumerate = number => [...Array(number).keys()].map(x => x + 1)
export const shuffle = string => [...string].sort(_ => Math.random() - 0.5).join('')
